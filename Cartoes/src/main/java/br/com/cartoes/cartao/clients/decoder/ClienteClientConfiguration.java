package br.com.cartoes.cartao.clients.decoder;

import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class ClienteClientConfiguration {
    @Bean
    public ErrorDecoder getClienteClientDecoder() {
        return new ClienteClientDecoder();
    }
}
