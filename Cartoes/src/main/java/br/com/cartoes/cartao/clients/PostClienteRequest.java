package br.com.cartoes.cartao.clients;

public class PostClienteRequest {

    private String nome;

    public PostClienteRequest(){}

    public PostClienteRequest(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}
