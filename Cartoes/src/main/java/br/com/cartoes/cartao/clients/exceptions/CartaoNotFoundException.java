package br.com.cartoes.cartao.clients.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Cartão não encontrado")
public class CartaoNotFoundException extends RuntimeException{

    private String msg;

    public CartaoNotFoundException(){
    }

    public CartaoNotFoundException(String msg){
        setMessage(msg);
    }

    @Override
    public String getMessage() {
        return this.msg;
    }

    public void setMessage(String msg) {
        this.msg = msg;
    }

}
