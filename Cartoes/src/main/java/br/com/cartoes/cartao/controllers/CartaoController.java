package br.com.cartoes.cartao.controllers;

import br.com.cartoes.cartao.DTOs.AtualizaCartaoAtivoDTO;
import br.com.cartoes.cartao.DTOs.CartaoResponseDTO;
import br.com.cartoes.cartao.DTOs.ExibeCartaoDTO;
import br.com.cartoes.cartao.DTOs.InsereCartaoRequestDTO;
import br.com.cartoes.cartao.clients.exceptions.CartaoNotFoundException;
import br.com.cartoes.cartao.models.Cartao;
import br.com.cartoes.cartao.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    CartaoService cartaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CartaoResponseDTO registraCompra(@RequestBody InsereCartaoRequestDTO insereCartaoRequest){
        return cartaoService.cadastraCartao(insereCartaoRequest);
    }

    @GetMapping("/{numero}")
    public ExibeCartaoDTO pesquisarPorNumero(@PathVariable(name = "numero") String numero) {
        return cartaoService.pesquisaPorNumeroCartao(numero);
    }

    @GetMapping("/info/{id}")
    public Cartao pesquisarPorId(@PathVariable(name = "id") int id) {
        try{
            return cartaoService.pesquisaPorId(id);
        } catch (CartaoNotFoundException e) {
            throw new CartaoNotFoundException("Cartão não encontrado");
        }
    }

    @GetMapping
    public Iterable<Cartao> exibeTodosOsCartoes() {
        return cartaoService.pesquisaCartoes();
    }

    @PatchMapping("/{numero}")
    public CartaoResponseDTO ativaCartao(@PathVariable(name = "numero") String numero,
                                         @RequestBody AtualizaCartaoAtivoDTO atualizaCartaoAtivoDTO){
            return cartaoService.atualizaPorNumeroCartao(numero, atualizaCartaoAtivoDTO);
    }
}
