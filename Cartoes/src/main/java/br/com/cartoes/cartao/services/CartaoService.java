package br.com.cartoes.cartao.services;

import br.com.cartoes.cartao.DTOs.AtualizaCartaoAtivoDTO;
import br.com.cartoes.cartao.DTOs.CartaoResponseDTO;
import br.com.cartoes.cartao.DTOs.ExibeCartaoDTO;
import br.com.cartoes.cartao.DTOs.InsereCartaoRequestDTO;
import br.com.cartoes.cartao.clients.Cliente;
import br.com.cartoes.cartao.clients.ClienteClient;
import br.com.cartoes.cartao.clients.exceptions.CartaoNotFoundException;
import br.com.cartoes.cartao.models.Cartao;
import br.com.cartoes.cartao.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class CartaoService {


    @Autowired
    ClienteClient clienteClient;

    @Autowired
    private CartaoRepository cartaoRepository;

    public CartaoResponseDTO cadastraCartao(InsereCartaoRequestDTO insereCartaoRequest) {

        Cliente cliente = new Cliente();

        Cartao insereCartao = new Cartao();

        cliente = clienteClient.getCliente(insereCartaoRequest.getClienteId());

        insereCartao.setAtivo(false);
        insereCartao.setClienteId(cliente.getId());
        insereCartao.setNumero(insereCartaoRequest.getNumero());

        Cartao novoCartao = cartaoRepository.save(insereCartao);

        CartaoResponseDTO response = new CartaoResponseDTO(
                novoCartao.getId(),
                novoCartao.getNumero(),
                novoCartao.getClienteId(),
                novoCartao.isAtivo()
        );

        return response;

    }

    public ExibeCartaoDTO pesquisaPorNumeroCartao(String numeroCartao) {
        try {
            Cartao cartaoPesquisado = cartaoRepository.findByNumero(numeroCartao);

            ExibeCartaoDTO exibeCartaoDTO = new ExibeCartaoDTO();
            exibeCartaoDTO.setId(cartaoPesquisado.getId());
            exibeCartaoDTO.setClienteId(cartaoPesquisado.getClienteId());
            exibeCartaoDTO.setNumero(cartaoPesquisado.getNumero());

            return exibeCartaoDTO;
        } catch (RuntimeException e) {
            throw new CartaoNotFoundException();
        }
    }

    public Cartao pesquisaPorId(int id) {
        Optional<Cartao> cartaoOptional = cartaoRepository.findById(id);
        if(cartaoOptional.isPresent()){
            Cartao cartao = cartaoOptional.get();
            return cartao;
        } else {
            throw new CartaoNotFoundException();
        }
    }

    public Iterable<Cartao> pesquisaCartoes() {
        return cartaoRepository.findAll();
    }

    public CartaoResponseDTO atualizaPorNumeroCartao(String numero, AtualizaCartaoAtivoDTO atualizaCartaoAtivoDTO) {

        CartaoResponseDTO response = new CartaoResponseDTO();
        Cartao cartaoAtualizado = new Cartao();

        if(cartaoRepository.existsByNumero(numero)){
            Cartao cartaoDB = cartaoRepository.findByNumero(numero);

            cartaoAtualizado = cartaoDB;
            cartaoAtualizado.setAtivo(atualizaCartaoAtivoDTO.isAtivo());
            cartaoRepository.save(cartaoAtualizado);

            response.setAtivo(cartaoAtualizado.isAtivo());
            response.setClienteId(cartaoAtualizado.getClienteId());
            response.setId(cartaoAtualizado.getId());
            response.setNumero(cartaoAtualizado.getNumero());
            return response;
        } else {
            throw new CartaoNotFoundException();
        }
    }
}
