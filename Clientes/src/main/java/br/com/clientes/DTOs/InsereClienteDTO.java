package br.com.clientes.DTOs;

public class InsereClienteDTO {

    private String nome;

    public InsereClienteDTO(){}

    public InsereClienteDTO(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
