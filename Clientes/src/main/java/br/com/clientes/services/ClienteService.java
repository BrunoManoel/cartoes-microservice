package br.com.clientes.services;

import br.com.clientes.DTOs.InsereClienteDTO;
import br.com.clientes.exceptions.ClienteNotFoundException;
import br.com.clientes.models.Cliente;
import br.com.clientes.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente cadastrarCliente(InsereClienteDTO clienteDTO) {
        Cliente cliente = new Cliente();
        cliente.setNome(clienteDTO.getNome());
        return clienteRepository.save(cliente);
    }

    public Cliente pesquisaClientePorId(int id){
        Optional<Cliente> clienteOptional = clienteRepository.findById(id);

        if(clienteOptional.isPresent()){
            Cliente cliente = clienteOptional.get();
            return cliente;
        }else {
            throw new ClienteNotFoundException();
        }
    }

}
