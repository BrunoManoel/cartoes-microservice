package br.com.faturas.fatura.clients;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;

public class FaturaPagaResponse {

    private int id;

    private double valorPago;

    @JsonProperty("pagoEm")
    private LocalDate dataDoPagamento;

    public FaturaPagaResponse() {
    }

    public FaturaPagaResponse(int id, double valorPago, LocalDate dataDoPagamento) {
        this.id = id;
        this.valorPago = valorPago;
        this.dataDoPagamento = dataDoPagamento;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getValorPago() {
        return valorPago;
    }

    public void setValorPago(double valorPago) {
        this.valorPago = valorPago;
    }

    public LocalDate getDataDoPagamento() {
        return dataDoPagamento;
    }

    public void setDataDoPagamento(LocalDate dataDoPagamento) {
        this.dataDoPagamento = dataDoPagamento;
    }
}
