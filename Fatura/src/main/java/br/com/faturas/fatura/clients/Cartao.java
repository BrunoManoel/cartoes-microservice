package br.com.faturas.fatura.clients;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class Cartao {

    private int id;

    private int clienteId;

    private String numero;

    private boolean ativo;

    public Cartao(){}

    public Cartao(int id, int clienteId, String numero, boolean ativo) {
        this.id = id;
        this.clienteId = clienteId;
        this.numero = numero;
        this.ativo = ativo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}
