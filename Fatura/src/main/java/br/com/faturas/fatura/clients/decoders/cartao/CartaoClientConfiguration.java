package br.com.faturas.fatura.clients.decoders.cartao;

import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class CartaoClientConfiguration {
    @Bean
    public ErrorDecoder getCartaoClientDecoder() {
        return new CartaoClientDecoder();
    }
}
