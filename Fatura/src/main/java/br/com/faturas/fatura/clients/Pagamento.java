package br.com.faturas.fatura.clients;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Pagamento {

    private int id;

    @JsonProperty("cartao_id")
    private int idCartao;

    private String descricao;

    private double valor;

    public Pagamento(){}

    public Pagamento(int id, int idCartao, String descricao, double valor) {
        this.id = id;
        this.idCartao = idCartao;
        this.descricao = descricao;
        this.valor = valor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdCartao() {
        return idCartao;
    }

    public void setIdCartao(int idCartao) {
        this.idCartao = idCartao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
