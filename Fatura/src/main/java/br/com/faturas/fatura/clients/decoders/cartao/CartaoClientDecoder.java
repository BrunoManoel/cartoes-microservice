package br.com.faturas.fatura.clients.decoders.cartao;

import br.com.faturas.fatura.clients.exceptions.CartaoNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class CartaoClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new ErrorDecoder.Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404) {
            return new CartaoNotFoundException();
        }
        return errorDecoder.decode(s, response);
    }

}
