package br.com.faturas.fatura.clients.decoders.pagamento;

import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class PagamentoClientConfiguration {
    @Bean
    public ErrorDecoder getPagamentoClientDecoder() {
        return new PagamentoClientDecoder();
    }
}
