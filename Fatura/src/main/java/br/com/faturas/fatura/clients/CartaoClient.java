package br.com.faturas.fatura.clients;

import br.com.faturas.fatura.clients.decoders.cartao.CartaoClientConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "CARTAO", configuration = CartaoClientConfiguration.class)
public interface CartaoClient {

    @GetMapping("/cartao/info/{idCartao}")
    Cartao pesquisaPorId(@PathVariable int idCartao);

}
