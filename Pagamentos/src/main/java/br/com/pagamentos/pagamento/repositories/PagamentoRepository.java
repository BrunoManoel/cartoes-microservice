package br.com.pagamentos.pagamento.repositories;

import br.com.pagamentos.pagamento.models.Cartao;
import br.com.pagamentos.pagamento.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {

    Iterable<Pagamento> findAllByIdCartao(int idCartao);

    void deleteByIdCartao(int idCartao);

}
