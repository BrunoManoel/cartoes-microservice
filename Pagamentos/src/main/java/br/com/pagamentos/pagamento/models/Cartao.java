package br.com.pagamentos.pagamento.models;

import javax.persistence.*;

public class Cartao {

    private int id;

    private int idCliente;

    private String numero;

    private boolean ativo;

    public Cartao(){}

    public Cartao(int id, int idCliente, String numero, boolean ativo) {
        this.id = id;
        this.idCliente = idCliente;
        this.numero = numero;
        this.ativo = ativo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getidCliente() {
        return idCliente;
    }

    public void setidCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}
