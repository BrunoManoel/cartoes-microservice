package br.com.pagamentos.pagamento.controllers;

import br.com.pagamentos.pagamento.DTOs.PagamentoResponseDTO;
import br.com.pagamentos.pagamento.DTOs.RealizaPagamentoRequestDTO;
import br.com.pagamentos.pagamento.clients.PagarFaturaResponse;
import br.com.pagamentos.pagamento.repositories.PagamentoRepository;
import br.com.pagamentos.pagamento.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/pagamentos")
public class PagamentoController {

    @Autowired
    PagamentoRepository pagamentoRepository;

    @Autowired
    PagamentoService pagamentoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public PagamentoResponseDTO registrarPagamento(@RequestBody @Valid RealizaPagamentoRequestDTO pagamentoDTO) {
        try{
            return pagamentoService.realizaPagamento(pagamentoDTO);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public List<PagamentoResponseDTO> pagamentosPorCartao(@PathVariable(name = "id") int id){
        try{
            return pagamentoService.buscaPagamentosPorCartao(id);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/{cartaoId}/pagar")
    @ResponseStatus(HttpStatus.OK)
    public PagarFaturaResponse pagarFatura(
            @PathVariable(name = "cartaoId") int cartaoId) {
        try{
            return pagamentoService.pagaFatura(cartaoId);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

}
