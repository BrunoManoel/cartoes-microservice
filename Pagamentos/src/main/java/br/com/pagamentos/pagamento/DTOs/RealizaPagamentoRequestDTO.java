package br.com.pagamentos.pagamento.DTOs;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RealizaPagamentoRequestDTO {

    @JsonProperty("cartao_id")
    private int idCartao;

    private String descricao;

    private double valor;

    public RealizaPagamentoRequestDTO(){}

    public RealizaPagamentoRequestDTO(int idCartao, String descricao, double valor) {
        this.idCartao = idCartao;
        this.descricao = descricao;
        this.valor = valor;
    }

    public int getIdCartao() {
        return idCartao;
    }

    public void setIdCartao(int idCartao) {
        this.idCartao = idCartao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
